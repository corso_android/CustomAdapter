package com.example.customadapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class CountryAdapter extends BaseAdapter {

    Context context;
    List<Country> countries;
    LayoutInflater inflter;

    public CountryAdapter(Context context, List<Country> countries) {
        this.context = context;
        this.countries = countries;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int i) {
        return countries.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if(view==null) {
            view = inflter.inflate(R.layout.layout_spinner, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            TextView textView = (TextView) view.findViewById(R.id.textView);
            viewHolder = new ViewHolder();
            viewHolder.imageView = imageView;
            viewHolder.textView = textView;
            view.setTag(viewHolder);
            viewHolder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogCountry dialogCountry = new DialogCountry(context, viewHolder.country);
                    dialogCountry.show();
                }
            });
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        Country country = countries.get(position);
        viewHolder.country = country;
        Picasso.with(context).load(viewHolder.country.getFlag()).into(viewHolder.imageView);
        viewHolder.textView.setText(viewHolder.country.getName());
        return view;
    }

    public class ViewHolder {

        ImageView imageView;
        TextView textView;
        Country country;
    }
}
