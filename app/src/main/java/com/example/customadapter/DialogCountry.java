package com.example.customadapter;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DialogCountry extends Dialog{

    @Bind(R.id.textView)
    TextView textView;

    public DialogCountry(@NonNull Context context, Country country) {
        super(context);
        setContentView(R.layout.layout_dialog_country);
        ButterKnife.bind(this);
        textView.setText(country.getCapital());
    }
}
